--1. Find all trades by a given trader on a given stock - for example the trader with ID=1 and the ticker 'MRK'. This involves joining from trader to position and then to trades twice, through the opening- and closing-trade FKs.

/*SELECT trader.ID, position.opening_trade_ID, position.closing_trade_ID, trade.stock
FROM trader
JOIN position ON trader.ID=position.trader_ID
JOIN trade ON position.opening_trade_ID=trade.ID
WHERE trader.ID =1 AND trade.stock = 'MRK'*/

--2. Find the total profit or loss for a given trader over the day, as the sum of the product of trade size and price for all sales, minus the sum of the product of size and price for all buys.

SELECT SUM(size*price * (CASE WHEN buy=1 THEN -1 ELSE 1 END)) as 'Profit/Loss'
FROM trade
JOIN position ON trade.ID = position.opening_trade_ID
JOIN trader ON trader.ID = position.trader_ID
WHERE trader.ID = 1
AND position.closing_trade_ID IS NOT NULL
AND position.closing_trade_ID != position.opening_trade_ID


--3. Develop a view that shows profit or loss for all traders.

/*	CREATE VIEW TradersProfitLoss AS
SELECT trader.first_name, trader.last_name, SUM(size*price * (CASE WHEN buy=1 THEN -1 ELSE 0 END)) as 'Profit/Loss'
FROM trade
JOIN position ON trade.ID = position.opening_trade_ID
JOIN trader ON trader.ID = position.trader_ID
WHERE position.closing_trade_ID IS NOT NULL
AND position.closing_trade_ID != position.opening_trade_ID
GROUP BY trader.ID, trader.first_name, trader.last_name		*/

------ Run View
SELECT * FROM TradersProfitLoss