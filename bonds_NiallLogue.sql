--1. Show all information about the bond with the CUSIP '28717RH95'.

SELECT *
FROM bond
WHERE CUSIP= '28717RH95';

--2.Show all information about all bonds, in order from shortest (earliest maturity) to longest (latest maturity).

SELECT * 
FROM bond
ORDER BY maturity ASC

-- 3. Calculate the value of this bond portfolio, as the sum of the product of each bond's quantity and price.

SELECT CUSIP, quantity*price as 'Value'
FROM bond

--4. Show the annual return for each bond, as the product of the quantity and the coupon. Note that the coupon rates are quoted as whole percentage points, so to use them here you will divide their values by 100.
SELECT CUSIP, quantity*price*(coupon/100) AS 'Annual Return'
FROM bond

--5. Show bonds only of a certain quality and above, for example those bonds with ratings at least AA2. (Don't resort to regular-expression or other string-matching tricks; use the bond-rating ordinal.)
SELECT CUSIP, rating
FROM bond
WHERE rating LIKE 'AA_' AND rating NOT LIKE 'AA3' 

--6. Show the average price and coupon rate for all bonds of each bond rating.
SELECT AVG(price) as 'Average Price', AVG(coupon) 'Average Coupon', rating
FROM bond
GROUP BY rating

--7. Calculate the yield for each bond, as the ratio of coupon to price. Then, identify bonds that we might consider to be overpriced, as those whose yield is less than the expected yield given the rating of the bond.
SELECT CUSIP, bond.rating, expected_yield, price*coupon as 'actual_yield', (price*coupon)-expected_yield as 'difference'
FROM bond
JOIN rating ON rating.rating = bond.rating